#include <iostream>
#include <cassert>

class Vector {
private:
    int* m_array;   // Pointer to dynamically allocated array
    int m_max_size; // Max size of the vector
    int m_size;     // Current size of the vector


public:
    // Constructor with max size
    Vector(int max_size);
    ~Vector();
  
    bool push_back(int value);
    int pop_back();
  
    int back();
    int front();
 
    int get_size();
    int get_max_size();
  
    void clear();
};

Vector::Vector(int max_size) {
    m_max_size = max_size;
    m_size = 0;
    m_array = new int[m_max_size];
}

Vector::~Vector() {
    delete[] m_array;
}


/*
Vector function implementations
*/
bool Vector::push_back(int value) {
    // if theres still space in the array add the value to the end
    if(m_size < m_max_size){
        m_array[m_max_size] = value;
        m_size++ ; // increase # of elements in current arr
        return true;
    }else{
        return false;
    }
}

int Vector::pop_back() {
    if(m_size != 0){
        m_size--;
        return true;
    }else{
        return false;
    }
}

int Vector::back(){
    if(m_size != 0){
        return m_array[m_size - 1];
    }else {
         throw std::out_of_range("Empty Vector");
    }
}

int Vector::front(){
    if(m_size != 0){
        return m_array[0];
    }else {
        throw std::out_of_range("Empty Vector");
    }
}

int Vector::get_size(){
    return m_size;
}

int Vector::get_max_size(){
    return m_max_size;
}

void Vector::clear(){
    m_size = 0;
}
