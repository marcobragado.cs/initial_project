#include "vector.hh"
#include <cassert>

int main()
{
    Vector v(5);

    assert(0 == v.get_size());
    assert(3 == v.get_max_size()); // failing
    assert(0 == v.pop_back());

    assert(true == v.push_back(123));
    assert(1 == v.get_size());
    assert(123 == v.pop_back()); // failing

    Vector v1(10);
    Vector v2 = v1; // returns 'free(): double free detected in tcache 2' error

    return 0;
}